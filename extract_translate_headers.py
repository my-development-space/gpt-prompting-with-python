# Note: This ia a chat-gpt 3.5-turbo generated code

import requests
from bs4 import BeautifulSoup
from googletrans import Translator

# Send a GET request to the web page
url = "https://medium.com/towards-data-science/pandas-2-0-a-game-changer-for-data-scientists-3cd281fcc4b4"
response = requests.get(url)

# Parse the HTML content using BeautifulSoup
soup = BeautifulSoup(response.content, 'html.parser')

# Find all the HTML headers
headers = soup.find_all(['h1', 'h2', 'h3', 'h4', 'h5', 'h6'])

# Translate the headers to German
translator = Translator()
translated_headers = [translator.translate(header.text, dest='de').text for header in headers]

# Save the translated headers into an HTML file
with open('translated_headers.html', 'w', encoding='utf-8') as file:
    for header in translated_headers:
        file.write(f"<h1>{header}</h1>\n")
