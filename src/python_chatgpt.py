import requests
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("api_key", type=str, help="Your private OpenAI API key")
parser.add_argument("prompt", type=str, help="The prompt to send to the OpenAI API")
parser.add_argument("output_filename", type=str, help="Name of the output file with extension")
args = parser.parse_args()

request_endpoint = "https://api.openai.com/v1/chat/completions"
request_apikey = args.api_key

request_headers = {
    "Content-Type": "application/json",
    "Authorization": "Bearer " + request_apikey
}
    
request_data = {
    "model": "gpt-3.5-turbo",
    "messages": [{"role": "user", "content": f"Write python script to {args.prompt}. Please give me only code and no text."}],
    "max_tokens": 500,
    "temperature": 0.7
}

response = requests.post(request_endpoint, headers=request_headers, json=request_data)

if response.status_code == 200:
    response_text = response.json()["choices"][0]["message"]["content"]
    with open(args.output_filename, "w") as file:
        file.write(response_text)
else:
    print(f"Request failed with the status code: {str(response.status_code)}")


# Example
# python src/python_chatgpt.py "your_api_key" "print Hello World" "hello_world.py"